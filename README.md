# Nuix RESTful Services Testing

## nuix_restful_test.py -h
```
usage: nuix_restful_test [-h] [-v] [-s] [-H host.example.com] [-u brains]
                         [-p brainspace1] [-P 8080] [-l Nuix Web Reviewer]
                         [--toplevel] [--diagnostics]

Usage for nuix_restful_test v2.0.3

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         Show verbose output.
  -s, --ssl             Use an SSL connection.
  -H host.example.com, --restful-hostname host.example.com
                        Hostname of the Nuix RESTful API server
  -u brains, --restful-username brains
                        Username for the Nuix RESTful API
  -p brainspace1, --restful-password brainspace1
                        Password for the Nuix RESTful API
  -P 8080, --port 8080  Nuix RESTful API service port
  -l Nuix Web Reviewer, --licence-type Nuix Web Reviewer
                        Licence Description string.
  --toplevel            load top-level items from the case selected. Beware
                        this can be HUGE!
  --diagnostics         Instead of viewing cases, generate a diagnostic log
                        archive: diagnostics.zip

```

Example:
`./nuix_restful_test.py -H server.domain -u demo -v --no-toplevel`


## Swagger UI
`http://hostname:8080/nuix-restful-service/svc/swagger-ui.html#/System`

## Authentication Request
HOST_URL=http://server:8080

```
curl -k -X PUT --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{
"licenceDescription": "Nuix Web Reviewer",
"password": "***********",
"username": "developer",
"workers": 0
}' "${HOST_URL}/nuix-restful-service/svc/v1/authenticatedUsers/login"

RESPONSE:
{
  "username" : "developer",
  "authToken" : "48bcbd7c-7755-4b58-8770-99d605ec61f8",
  "licenceDescription" : "Nuix Web Reviewer"
}
```
TOKEN='48bcbd7c-7755-4b58-8770-99d605ec61f8'

## Case List Request
```
curl -kv -X GET --header 'Accept: application/json' --header "nuix-auth-token: ${TOKEN}" "${HOST_URL}/nuix-restful-service/svc/v1/inventory/digest"
```

## Case Details
```
curl -X GET --header Accept: application/json' --header "nuix-auth-token: ${TOKEN}" \
"{$HOST_URL}/nuix-restful-service/svc/v1/cases/${CASEID}"

```

## Case Items
```
curl -X GET --header 'Accept: application/json' --header "nuix-auth-token: ${TOKEN}" \
"${HOST_URL}/nuix-restful-service/svc/v1/cases/${CASEID}/topLevelItems?fields=fileSize&fields=fileType&fields=guid&fields=id&fields=isBinaryAvailable&fields=isPhysicalFile&fields=isTextAvailable&fields=name&fields=type"
```
