#!/usr/bin/env python
'''
Description: Test connectivity from a Brainspace application server to the Nuix Restful API service
Author Russ Cook <russ.cook@appgate.com>
Change Log:
2018-05-06: Initial release
2018-05-08: Supress self-signed certificate warnings. Change help for licence name to correct description.
2019-10-11: Add support for handling unopened cases
2020-10-09: Significant re-write. Python2 and Python3 suport. Does not exit after case details. Gathers diagnostic logs
2020-10-12: Enhanced error handling. Add Item Count to case details. Fixed a few bugs and code formatting
2020-10-12: Enable case size calcuating and display in human readable format
2020-10-13: Fixed a bug with older API versions
2020-10-19: Add licence type by shortName only. API login now uses both licenceDescription and licenceShortName to comply with API 8.6, 8.8
'''
####
## Environment Variables
####
## export NUIX_REST_HOST="nuixrest.example.com"
## export NUIX_REST_PORT=8080
## export NUIX_REST_SSL=True
## export NUIX_LICENSE_TYPE="Nuix Web Reviewer"
## export NUIX_REST_USER=brains
## export NUIX_REST_PASS=password

###
## Import Modules
###
import os,sys,signal,time,datetime
import json
import argparse
import requests
from operator import itemgetter
from pprint import pprint
import urllib3
import logging
try:
	urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
except:
	pass

###
## Global Static Variables
###
## Name of this application
app_name = 'nuix_restful_test'
app_version = '2.0.7'

nuix_licences = {
	"web-reviewer": "Nuix Web Reviewer",
	"enterprise-reviewer": "Nuix eDiscovery Reviewer",
	"enterprise-workstation": "Nuix eDiscovery Workstation"
}
licence_choices = []
for shortName in nuix_licences:
	licence_choices.append(shortName)
###
## Process command-line arguments
###
parser = argparse.ArgumentParser(prog=app_name,description='Usage for {} v{}'.format(app_name,app_version))
parser.add_argument('-v','--verbose', action="count", default=False, help='Show verbose output.')
parser.add_argument('-s','--ssl', action="store_true", default=False, help='Use an SSL connection.')
parser.add_argument('-H','--restful-hostname',  metavar='host.example.com', help='Hostname of the Nuix RESTful API server')
parser.add_argument('-u','--restful-username',  metavar='brains', help='Username for the Nuix RESTful API')
parser.add_argument('-p','--restful-password',  metavar='brainspace1', help='Password for the Nuix RESTful API')
parser.add_argument('-P','--port', metavar='8080', default=8080, type=int ,help='Nuix RESTful API service port')
parser.add_argument('-l','--licence-type', choices=licence_choices, metavar='web-reviewer', default='web-reviewer', help='Licence Short Name. Options: {}'.format(licence_choices))
parser.add_argument('--toplevel', action="store_true", default=False, help='load top-level items from the case selected. Beware this can be HUGE!')
parser.add_argument('--diagnostics', action="store_true", default=False, help='Instead of viewing cases, generate a diagnostic log archive: diagnostics.zip')
args = parser.parse_args()

###
## Global variable overrides
###
verbose = args.verbose
VARS={}
VARS['NUIX_REST_HOST'] = os.environ['NUIX_REST_HOST'] if 'NUIX_REST_HOST' in os.environ else args.restful_hostname
VARS['NUIX_REST_PORT'] = os.environ['NUIX_REST_PORT'] if 'NUIX_REST_PORT' in os.environ else str(args.port)
VARS['NUIX_REST_SSL'] = bool(os.environ['NUIX_REST_SSL']) if 'NUIX_REST_SSL' in os.environ else args.ssl
VARS['NUIX_LICENSE_TYPE'] = os.environ['NUIX_LICENSE_TYPE'] if 'NUIX_LICENSE_TYPE' in os.environ else args.licence_type
VARS['NUIX_REST_USER'] = os.environ['NUIX_REST_USER'] if 'NUIX_REST_USER' in os.environ else args.restful_username
VARS['NUIX_REST_PASS'] = os.environ['NUIX_REST_PASS'] if 'NUIX_REST_PASS' in os.environ else args.restful_password


""" configure console logging """


if args.verbose > 2:
	logging.basicConfig(stream=sys.stdout,format='[%(levelname)s] (%(filename)s:%(lineno)d) %(message)s',level=logging.DEBUG)
	log = logging.getLogger()
	log.debug("Logging Level: DEBUG")
else:
	logging.basicConfig(stream=sys.stdout,format='[%(levelname)s] %(message)s',level=logging.INFO)
	log = logging.getLogger()


if args.verbose > 1:
	log.setLevel(logging.DEBUG)
	logging.getLogger("urllib2").setLevel(logging.WARNING)

elif args.verbose == 1:
	log.setLevel(logging.DEBUG)
	logging.getLogger("urllib2").setLevel(logging.WARNING)

else:
    logging.getLogger("urllib2").setLevel(logging.CRITICAL)


###
############ INTERNAL FUNCTIONS ##################
###
def signal_handler(signal, frame):
	heDed(0)


def heDed(signal=1):
	logout()
	print("Goodbye :(")
	sys.exit(signal)
	os._exit(os.EX_OK)


def convert_unit_human(size_in_bytes):
	""" Convert the size from bytes to other units like KB, MB or GB"""
	if size_in_bytes > 1073741824:
		return str(round(size_in_bytes/(1024*1024*1024),1)) + ' GB'
	elif size_in_bytes > 1048576:
		return str(round(size_in_bytes/(1024*1024),1)) + ' MB'
	elif size_in_bytes > 1024:
		return str(round(size_in_bytes/1024),1) + ' KB'
	else:
		return str(size_in_bytes) + 'Bytes'


def getInput(prompt_message):
	try:
		## Python2
		response = raw_input(prompt_message)
	except:
		## Python3
		response = input(prompt_message)

	return response

def printTable(myDict, colList=None):
	""" Pretty print a list of dictionaries (myDict) as a dynamically sized table.
	If column names (colList) aren't specified, they will show in random order.
	"""
	if not colList: colList = list(myDict[0].keys() if myDict else [])
	myList = [colList] # 1st row = header
	for item in myDict: myList.append([str(item[col] or '') for col in colList])
	colSize = [max(map(len,col)) for col in zip(*myList)]
	formatStr = ' | '.join(["{{:<{}}}".format(i) for i in colSize])
	myList.insert(1, ['-' * i for i in colSize]) # Seperating line
	for item in myList: print(formatStr.format(*item))


def printHeader(pre='',post='',title='',char='#',pad_len=85):
	padding = ''.rjust(pad_len,char)

	if title != '':
		# determine the length of padding on each side of title
		lpad_len = int( ( pad_len - (len(title) + 2) ) / 2)
		padding = '{} {} {}'.format(''.rjust(lpad_len,char), title, ''.rjust(lpad_len,char) )
		# Add more padding if the new length is less thatn pad_len
		diff = pad_len - len(padding)
		if diff > 0:
			padding = "{}{}".format(padding,''.rjust(diff,char))

	print("{}{}{}".format(pre,padding,post))
	return True


def printCaseList(cases, verbose=0):
	printHeader('\n')
	printHeader('','','[ Nuix Cases ]')
	index=0

	for case in cases:
		print(str(index) + ') ' + case['name'])
		index += 1

	printHeader('','\n')
	return getInput("What case would you like to see details about? (ctrl-c to exit): ")


def printCaseDetail(case_detail, verbose=0):
	print_fields = [
		{'label': 'Name', 'id': 'caseName'},
		{'label': 'Description', 'id': 'caseDescription'},
		{'label': 'Case ID', 'id': 'caseId'},
		{'label': 'Date Created', 'id': 'caseCreationDate'},
		{'label': 'Investigator', 'id': 'caseInvestigator'},
		{'label': 'Path', 'id': 'casePath'},
		{'label': 'Size', 'id': 'caseSize'},
		{'label': 'Item Count', 'id': 'itemCount'},
		{'label': 'Entity Types', 'id': 'entityTypes'}
	]
	label_length = 14

	printHeader("\n")

	for field in print_fields:
		# Calculate the length of padding to format on screen
		rpad = (label_length - len(field['label']))

		if field['id'] in case_detail:
			print("{}:{}{}".format(field['label'], ''.ljust(rpad, ' '), case_detail[field['id']]))

	printHeader('','\n')
	return True


###
############ REST API FUNCTIONS ##################
###
def showApiError(err_dict):
	## Print API error messages to the logger with some formatting
	#pprint(err_dict)
	err_msg = []

	if 'errorCode' in err_dict:
		err_msg.append(err_dict['errorCode'] + ":")
	elif 'error' in err_dict:
		err_msg.append(err_dict['error'] + ':')

	if 'message' in err_dict and err_dict['message'] is not None:
		err_msg.append(err_dict['message'])
	elif 'userMessage' in err_dict and err_dict['userMessage'] is not None:
		err_msg.append(err_dict['userMessage'])
	else:
		if 'developerMessage' in err_dict:
			err_msg.append(err_dict['developerMessage'])

	log.error(' '.join(err_msg) )

	if 'additionalInfo' in err_dict and err_dict['additionalInfo'] != '':
		log.error("Additional Info: {}".format(err_dict['additionalInfo']))


def logout():
	if 'authToken' in VARS:
		log.debug("Logging out and clearing authentication token")
		headers = { "nuix-auth-token": VARS['authToken']}
		logout_response = requests.delete(proto + '://' + VARS['NUIX_REST_HOST'] + ':' + VARS['NUIX_REST_PORT'] + '/nuix-restful-service/svc/v1/authenticatedUsers/' + VARS['NUIX_REST_USER'] + '?forceSynchronous=false', verify=False, headers=headers)
		os._exit(os.EX_OK)

		if logout_response.status_code != 200:
			log.error("Error logging out" + VARS['NUIX_REST_USER'])
			log.error(json.loads(logout_response.text)['userMessage'])
			heDed()


def connectNuixHostApi(verbose=0):
	## Verify that we can communicate with the Restful API
	global VARS
	verifyNuixHostReachable(VARS['base_url'],verbose)

	## Attempt authentication with API server
	VARS['authToken'] = getAuthenticationToken(VARS['base_url'], VARS['NUIX_LICENSE_TYPE'], VARS['NUIX_REST_USER'], VARS['NUIX_REST_PASS'], verbose)

	## Get the API Server's Information
	getApiInfo(VARS['base_url'], VARS['authToken'], verbose)

	return True


def verifyNuixHostReachable(base_url,verbose=0):
	## Verify that we can communicate with the Restful API
	health_headers = {"Accept": "application/json"}
	log.debug("Connecting to Nuix API")
	try:
		health_response = requests.get(base_url + '/nuix-restful-service/svc/v1/system/health', verify=False, headers=health_headers)
	except requests.exceptions.RequestException as error:
		log.error("Unable to connect to Restful Server: {}".format(base_url))
		log.error("Details: {}\n".format(error))
		heDed()

	if health_response.status_code != 200:
		showApiError(json.loads(health_response.text))
		heDed()
	else:
		log.debug("API Connection Successful!")
		return True



def getAuthenticationToken(base_url, licence_type, rest_user, rest_pass, verbose=0):
	## Attempt authentication with API server
	log.debug("Authenticating with Nuix API")

	auth_data = { "licenceDescription": nuix_licences[licence_type], "licenceShortName": licence_type, "username": rest_user, "password": rest_pass, "workers": 0}
	auth_headers = {"Content-type": "application/json", "Accept": "application/json"}

	try:
		auth_response = requests.put(base_url + '/nuix-restful-service/svc/v1/authenticatedUsers/login', verify=False, data=json.dumps(auth_data), headers=auth_headers)
		auth_data = json.loads(auth_response.text.encode('utf-8', 'replace'))
		#pprint(auth_response.status_code)
		#pprint(auth_data)
	except:
		log.error("Unable to authenticate to the RESTFul Server: {}".format(base_url))
		heDed()

	if auth_response.status_code == 201:
		log.debug("Authentication Successful!")
		log.debug("Username: {}".format(auth_data['username']))
		## The name of the license type parameter changes after version 8.6
		if 'licenceDescription' in auth_data: # version >= 8.8
			log.debug("licence Type: {}".format(auth_data['licenceDescription']))
		if 'licenseShortName' in auth_data: #version < 8.8
			log.debug("licence Type: {}".format(auth_data['licenseShortName']))
		log.debug("Authentication Token: {}".format(auth_data['authToken']))

		return auth_data['authToken']

	elif auth_response.status_code == 400:
		if (auth_data['developerMessage'].find('requested licence could not be identified')) > 0:
			log.error("Invalid licence type requested: {}".format(license_type))
		else:
			showApiError(auth_data)
	else:
		showApiError(auth_data)
	heDed()



def getApiInfo(base_url, auth_token, verbose=0):
	## Fetch the server's version and details from the API
	log.debug("Getting the Nuix API Information")
	api_headers = { "Accept": "application/json", "nuix-auth-token": auth_token}
	about_response = requests.get(base_url + '/nuix-restful-service/svc/v1/about', verify=False, headers=api_headers)
	about_api = json.loads(about_response.text.encode('utf-8', 'replace'))
	#pprint(about_response.text)

	## Calculate the server's uptime
	startup_time = datetime.datetime.fromtimestamp(about_api['startupTime']//1000.0)
	delta = datetime.datetime.today() - startup_time
	dhours = round(delta.seconds/3600)
	dminutes = round (( delta.seconds - (dhours * 3600) ) / 60 )

	## Print API Server's Information
	printHeader('','','[ Nuix API ]')
	print("Server:         {}".format(about_api['server']))
	print("API Version:    {}".format(about_api['nuixRestfulVersion']))
	print("Server ID:      {}".format(about_api['serverId']))
	print("Engine Version: {}".format(about_api['engineVersion']))
	print("Licence Source: {}".format(about_api['licenceSource']))
	print("Uptime:         {} days {} hours {} minutes".format(delta.days, dhours, dminutes ))
	printHeader()
	return True


def getCases(base_url, auth_token, verbose=0):
	## Load the list of cases from the API
	log.debug("Getting the Case list from the Nuix API")
	api_headers = { "Accept": "application/json", "nuix-auth-token": auth_token}
	cases_response = requests.get(base_url + '/nuix-restful-service/svc/v1/inventory/digest', verify=False, headers=api_headers)

	cases = json.loads(cases_response.text.encode('utf-8', 'replace'))
	#pprint(cases_response.text)

	if len(cases) == 0:
		log.error("No Nuix Cases found for review... Exiting")
		heDed()
	else:
		case_index = None
		while case_index is None or case_index == '':

			case_index = printCaseList(cases, verbose)

			try:
				case = cases[int(case_index)]
			except:
				log.warning("Invalid case number entered.")
				case_index = None
				continue

			getCaseInfo(base_url, auth_token, case)
			qnext = getInput("Quit: 'q' or enter to continue:")
			if qnext == 'q':
				heDed(0)

			case_index = None

def getCaseDetail(base_url, auth_token, case, uri, field_name=None):
	try:
		api_headers = { "Accept": "application/json", "nuix-auth-token": auth_token}
		case_response = requests.get(base_url + '/nuix-restful-service/svc/v1/cases/' + case['caseId'] + uri, verify=False, headers=api_headers)
		case_detail = json.loads(case_response.text.encode('utf-8','replace'))

		if field_name is not None and field_name in case_detail:
			return case_detail[field_name]
		else:
			return case_detail

	except:
		return False


def getCaseInfo(base_url, auth_token, case):
	## Load the case details from the API
	log.info("Loading case details for Case: {} (Case ID: {})".format(case['name'], case['caseId']))

	try:
		api_headers = { "Accept": "application/json", "nuix-auth-token": auth_token}
		case_response = requests.get(base_url + '/nuix-restful-service/svc/v1/cases/' + case['caseId'] + '?calculateCaseSize=true', verify=False, headers=api_headers)
		case_detail = json.loads(case_response.text.encode('utf-8','replace'))
		#print(case_response.status_code)
		#pprint(case_detail)
	except:
			log.error('Failed to Load Case Details')
			showApiError(case_detail)

	if case_response.status_code != 200:
		log.error("Failed to get Case details.")
		showApiError(case_detail)

	else:
		if 'caseCreationDate' in case_detail:
			case_detail['caseCreationDate'] = datetime.date.fromtimestamp(case_detail['caseCreationDate']//1000.0).strftime( "%b %d %Y")
		if 'caseSize' in case_detail:
			case_detail['caseSize'] = convert_unit_human(case_detail['caseSize'])
		case_detail['itemCount'] = getCaseDetail(base_url, auth_token, case, '/count', 'count')
		#case_detail['entityTypes'] = ', '.join(getCaseDetail(base_url, auth_token, case, '/entityTypes'))
		printCaseDetail(case_detail)

		if args.toplevel:
			print("Loading top-level items. This may take a minute....")

			field_list=['id','name','fileSize','fileType','kind','isTextAvailable']
			fields = '&fields='.join(field_list)
			try:
				casetli_response = requests.get(base_url + '/nuix-restful-service/svc/v1/cases/' + case['caseId'] + '/topLevelItems?fields=' + fields , verify=False, headers=api_headers)
				case_topLevelItems = json.loads(casetli_response.text.encode('ascii','ignore'))
				#pprint(case_topLevelItems)
				#print(fields)
			except:
					log.error('Failed to Load Case Details')

			try:
				printTable(case_topLevelItems,field_list)
			except UnicodeEncodeError as e:
				#pprint(e)
				log.error("An error occured formating data for screen output. It may contain unicode characters.")


def getDiagnostics(base_url, auth_token, verbose):
	## Fetch the server's diagnostic logs from the API
	api_headers = { "Accept": "application/zip", "nuix-auth-token": auth_token}
	diag_response = requests.get(base_url + '/nuix-restful-service/svc/v1/system/diagnostics', verify=False, headers=api_headers)
	if diag_response.status_code != 200:
		log.error("HTTP Status Code: {}".format(diag_response.status_code))
		log.error("An Unknown error occured. Failed to get the server's diagnostic logs")
		return False

	if 'Content-Disposition' in diag_response.headers:
		filename = diag_response.headers['Content-Disposition'].split('=')[1]
		#pprint(diag_response.headers['Content-Disposition'])
		diag_file = open(filename, "wb")
		diag_file.write(bytearray(diag_response.content))
		diag_file.close()
	log.info("Diagnostic log archive saved to file: {}".format(filename))



###
############ MAIN FUNCTION ##################
###
if __name__ == "__main__":
	signal.signal(signal.SIGINT, signal_handler)


	if VARS['NUIX_REST_HOST'] == None:
		log.error("Restful Service hostname is required",True)
		heDed()
	if VARS['NUIX_REST_USER'] == None:
		log.error("Restful Service username is required",True)
		heDed()
	if VARS['NUIX_REST_PASS'] == None:
		os.system("stty -echo")
		VARS['NUIX_REST_PASS'] = getInput("Please enter the password for the Nuix user(" + VARS['NUIX_REST_USER'] + "): ")
		os.system("stty echo")
		print('\n')

	printHeader('\n')
	printHeader('','','- {} v{} -'.format(app_name, app_version))

	log.debug("Run Variables for {} {}".format(app_name, app_version))

	proto = 'https' if VARS['NUIX_REST_SSL'] else 'http'
	VARS['base_url'] = proto + '://' + VARS['NUIX_REST_HOST'] + ':' + VARS['NUIX_REST_PORT']
	pprint(VARS) if verbose > 1 else ''

	connectNuixHostApi(verbose)

	if args.diagnostics:
		## Save system diagnostics to a file
		getDiagnostics(VARS['base_url'], VARS['authToken'], verbose)
	else:
		## Get a list of cases
		cases = getCases(VARS['base_url'], VARS['authToken'], verbose)


	## Exit cleanly
	logout()
	sys.exit(0)
